# LIS4381

## William Ward

### Assignment 5 


#### Assignment Screenshots:

* Screenshot of SQL petstore database
* A Web application where a petstore owner can record, track, and maintain relevant
company data
* Attributes for entities such as people, places, or organizations

*Screenshot of error page*:

![img1.png](img/img1.png)

* Screenshot of the error page
* User makes error and this page displays

*Screenshot of applications first user interface*:

![img2.png](img/img2.png)

* Screenshot of server side validation
* Displays user-entered data
* Permits user to add data

*LINK*:


[lis 4381 web app](http://localhost/repos/lis4381/)