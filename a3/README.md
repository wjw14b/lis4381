# LIS4381

## William Ward

### Assignment 3 


#### Assignment Screenshots:

* Screenshot of SQL petstore database
* A Web application where a petstore owner can record, track, and maintain relevant
company data
* Attributes for entities such as people, places, or organizations

*Screenshot of mySQL*:

![img5.png](img/img5.png)

* Screenshot of Ticket Value application
* User selects band and inputs ticket value

*Screenshot of applications first user interface*:

![img3.png](img/img3.png)

* Screenshot of response to the input of ticket value based on band
* The calculation of the inputs based on number of tickets and price of band
* Application shows the response

*Screenshot of applications second user interface*:

![img4.png](img/img4.png)

*mwb file:*
[mwb file](links/petstore.mwb)

*sql file:*
[sql file](links/a3.sql)
