# LIS4381 MOBILE APP DEVELOPMENT

## William Ward

*Assignments*

1. [A1 README.md](a1/README.md "My A1 README.md file")

-install AMPPS

-install JDK

-Install Android Studio and Create my First App

-Provide Screenshots and Installations

2. [A2 README.md](a2/README.md "My A2 README.md file")

-Build Recipe beginning App

-import Java Classes

3. [A3 README.md](a3/README.md "My A3 README.md file")
4. [P1 README.md](p1/README.md "My P1 README.md file")

-Build Business Card App

5. [A4 README.md](a4/README.md "My A4 README.md file")
6. [A5 README.md](a5/README.md "My A5 README.md file")

-Build a Repo and RSS Feed

7. [P2 README.md](p2/README.md "My P2 README.md file")