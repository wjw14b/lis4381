<DOCTYPE HTML>
<html>
<head>
	<meta charset= "utf-8">
	<title>Using RSS Feeds</title>
</head>

<body>
<?php
//What is Rss? http://www.whatisrss.com/
//RSS Specification: http://www/rss-specifications.com/rss-specifications.htm
//Anatomy of an RSS feed: http://www.webreference.com/authoring/languages/xml/rss/feeds/index.html
//tutorial: http://www.htmlgoodies.com/beyond/xml/article.php/3691751

//example:
//$url = 'http://feeds.reuters.com/Reuters/domesticNews';
$url = 'http://feeds.reuters.com/Reuters/domesticNews';
$feed = simplexml_load_file($url, 'SimpleXMLIterator');
echo "<h2>" . $feed->channel->description . "</h2><ol>";

$filtered = new LimitIterator($feed->channel->item, 0, 10);
foreach ($filtered as $item) { ?>
	<h4><li><a href="<?= $item->link; ?>" target="_blank"><?= $item->title;?></a></li></h4>
	<?php
	//must set default time zone
	date_default_timezone_set('America/New_York');

	$date = new DateTime($item->pubDate);
	$date->setTimeZone(new DateTimeZone('America/New_York'));
	$offset = $date->getOffset();
	$timezone = ($offset == -14400) ?' EDT' :' EST';
//   echo $date
	echo $date->format('M j, Y, g:ia') . $timezone;
?>
	<p><?php echo $item->description; ?></p>
<?php } ?>
	<!-- end ordered list -->
	</ol>
</body>
</html>