# LIS4381

## William Ward

### Project 2 


#### Project Screenshots:

* Screenshot of SQL petstore database
* A Web application where a petstore owner can record, track, and maintain relevant
company data
* Attributes for entities such as people, places, or organizations

*Screenshot of index.php page*:

![img1.png](img/img1.png)

* Screenshot of the index page
* Displays petstore table

*Screenshot of editing page*:

![img2.png](img/img2.png)

* Screenshot of editing page
* Displays page to edit table after clicking edit
* Permits user to edit data

*Screenshot of editing error page*:

![img3.png](img/img3.png)

* Screenshot of editing error page
* Displays error when adding a restricted character

*Screenshot of home page*:

![img4.png](img/img4.png)

* Screenshot of home page
* Displays carousel with pictures and comment discription

*Screenshot of RSS Feed page*:

![img5.png](img/img5.png)

* Screenshot of RSS Feed page
* Displays feed from U.S. news sources

*LINK*:


[lis 4381 web app](http://localhost/repos/lis4381/)